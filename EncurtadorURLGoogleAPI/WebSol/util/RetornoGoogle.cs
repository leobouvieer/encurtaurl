﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSol.util
{
    public class RetornoGoogle
    {
        public string kind { get; set; }
        public string id { get; set; }
        public string longURL { get; set; }
        public string status { get; set; }

    }
}