﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;


namespace WebSol.util
{
    public class UtilClass
    {
        public static string EncurtarURL(string urlCompleta)
        {
            var retornoGoogle = new RetornoGoogle();

            WebClient webClient = null;
            var dadosGoogle = new DadosPublicacao
            {
                ApiKeyGoogle = ConfigurationManager.AppSettings["keyGoogle"],
                UrlApiGoogle = "https://www.googleapis.com/urlshortener/v1/url?key="
            };

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(dadosGoogle.UrlApiGoogle + dadosGoogle.ApiKeyGoogle);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(JsonConvert.SerializeObject(new { longUrl = urlCompleta }));
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();


                using (var reader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var retorno = reader.ReadToEnd();
                    retornoGoogle = JsonConvert.DeserializeObject<RetornoGoogle>(retorno);
                }

            }
            catch (Exception e)
            {


            }
            return retornoGoogle.id;


        }

    }
}