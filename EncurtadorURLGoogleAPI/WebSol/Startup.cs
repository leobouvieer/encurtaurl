﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebSol.Startup))]
namespace WebSol
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
