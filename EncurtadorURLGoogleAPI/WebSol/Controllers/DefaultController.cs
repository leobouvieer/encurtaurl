﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSol.util;

namespace WebSol.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default/Create
        public ActionResult Create()
        {
            var retornoGoogle = new RetornoGoogle();
            ModelState.Clear();

            return View(retornoGoogle);
        }

        // POST: Default/Create
        [HttpGet]
        public ActionResult Create([FromBody]string url)
        {
            try
            {
                var urlCurta = UtilClass.EncurtarURL(url);
                ViewBag.urlCurta = urlCurta;
                var retornoGoogle = new RetornoGoogle
                {
                    id = urlCurta,
                    status = "ok"
                };
                ModelState.Clear();
                return View(retornoGoogle);
            }
            catch
            {
                return View();
            }
        }

    }
}
